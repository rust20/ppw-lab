$(document).ready(function(){
	$(".chat-msg").keypress(enterKey);
	initTheme();
	$('.my-select').select2({
			'data': JSON.parse(window.localStorage.getItem('themes'))
	});
	$('.apply-button').on('click', function(){  // sesuaikan class button
		applyTheme($('.my-select').val());
	});

});

function initTheme(){
	var themes = [
		{"id":0,"text":"Red","bcgColor":"#F44336","fontColor":"#FAFAFA"},
		{"id":1,"text":"Pink","bcgColor":"#E91E63","fontColor":"#FAFAFA"},
		{"id":2,"text":"Purple","bcgColor":"#9C27B0","fontColor":"#FAFAFA"},
		{"id":3,"text":"Indigo","bcgColor":"#3F51B5","fontColor":"#FAFAFA"},
		{"id":4,"text":"Blue","bcgColor":"#2196F3","fontColor":"#212121"},
		{"id":5,"text":"Teal","bcgColor":"#009688","fontColor":"#212121"},
		{"id":6,"text":"Lime","bcgColor":"#CDDC39","fontColor":"#212121"},
		{"id":7,"text":"Yellow","bcgColor":"#FFEB3B","fontColor":"#212121"},
		{"id":8,"text":"Amber","bcgColor":"#FFC107","fontColor":"#212121"},
		{"id":9,"text":"Orange","bcgColor":"#FF5722","fontColor":"#212121"},
		{"id":10,"text":"Brown","bcgColor":"#795548","fontColor":"#FAFAFA"}
	];

	window.localStorage.setItem('themes', JSON.stringify(themes));
	if(window.localStorage.getItem('selectedTheme') == null) {
		applyTheme(3);
	} else {
		applyTheme(JSON.parse(window.localStorage.getItem('selectedTheme')).id);
	}
}

function enterKey (key){
	if (key.which == 13) {
		var message = "<p class='msg-send'>" + $(".chat-msg").val() + "</p>";
		$(".msg-insert").append(message);
		$(".chat-msg").val("");
		console.log("test");
		return false;
	}
}



function applyTheme(id){
	var themes = JSON.parse(window.localStorage.getItem('themes'));
	var theme = themes.filter(function(item) { return item.id == id; })[0];
	window.localStorage.setItem('selectedTheme', JSON.stringify(theme));
	$('body').css('background-color', theme.bcgColor);
	$('#calc-buttons').css('color', theme.fontColor);
}

function testFunction() {
	var button_8 = $('button:contains("8")');
	console.log(button_8);
}


// Calculator
var erase = false;

var go = function(x) {
	var print = document.getElementById('print');
	if (x === 'ac') {
		print.value = "";
	} else if (x === 'eval') {
		print.value = Math.round(evil(print.value) * 10000) / 10000;
		erase = true;
	} else if (x === 'log') {
		print.value = Math.round(evil('Math.log10(' + print.value + ')') * 10000) / 10000;
		erase = true;
	} else if (x === 'sin') {
		print.value = Math.round(evil('Math.sin(' + print.value + ')') * 10000) / 10000;
		erase = true;
	} else if (x === 'tan') {
		print.value = Math.round(evil('Math.tan(' + print.value + ')') * 10000) / 10000;
		erase = true;
	} else {
		print.value += x;
	}
};

function evil(fn) {
	return new Function('return ' + fn)();
}
