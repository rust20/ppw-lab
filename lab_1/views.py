from django.shortcuts import render
from datetime import datetime, date
# Enter your name here
mhs_name = 'R. Azka Ali Fazagani' # TODO Implement this
curr_year = int(datetime.now().strftime("%Y"))
birth_date = date(1998, 2, 15) #TODO Implement this, format (Year, Month, Date)
# Create your views here.
def index(request):
    response = {'name': mhs_name, 'age': calculate_age(1998)}
    return render(request, 'index.html', response)

def calculate_age(birth_year):
    return curr_year - birth_year if birth_year <= curr_year else 0
