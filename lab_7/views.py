from django.shortcuts import render
from django.http import HttpResponseRedirect, JsonResponse
from django.views.decorators.csrf import csrf_exempt
from django.forms.models import model_to_dict
from .models import Friend
from .api_csui_helper.csui_helper import CSUIhelper
#  import os

response = {}
csui_helper = CSUIhelper()


def index(request):
    page = request.GET.get("page", '1')
    try:
        page = int(page)
        csui_response = csui_helper.instance.get_mahasiswa_list(page=page)
        mahasiswa_list = csui_response['results']
    except Exception:
        return HttpResponseRedirect("/lab-7/")

    friend_list = Friend.objects.all()
    html = 'lab_7/lab_7.html'
    response = {"mahasiswa_list": mahasiswa_list,
                "friend_list": friend_list,
                "next_page": page + 1,
                "pref_page": page - 1}
    return render(request, html, response)

def friend_list(request):
    friend_list = Friend.objects.all()
    response['friend_list'] = friend_list
    html = 'lab_7/daftar_teman.html'
    return render(request, html, response)


@csrf_exempt
def friend_list_json(request):  # update
    friends = [model_to_dict(obj) for obj in Friend.objects.all()]
    return JsonResponse({"results": friends}, content_type='application/json')

@csrf_exempt
def add_friend(request):
    if request.method == 'POST':
        npm = request.POST['npm']
        name = request.POST['name']
        if not is_npm_taken(npm):
            friend = Friend(friend_name=name, npm=npm)
            friend.save()
            return JsonResponse(friend.as_dict())
        return JsonResponse({'message': 'Mahasiswa sudah ditambahkan '
                             'sebagai teman'}, status=400)


@csrf_exempt
def delete_friend(request, friend_id):
    Friend.objects.filter(id=friend_id).delete()
    return JsonResponse({'message': 'mahasiswa dihapus dari daftar teman'},
                        status=204)

@csrf_exempt
def validate_npm(request):
    npm = request.POST.get('npm', None)
    data = {
        'is_taken': is_npm_taken(npm)
    }
    return JsonResponse(data)

def is_npm_taken(npm):
    return Friend.objects.filter(npm=npm).exists()
